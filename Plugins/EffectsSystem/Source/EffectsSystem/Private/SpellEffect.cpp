// Fill out your copyright notice in the Description page of Project Settings.


#include "SpellEffect.h"

void USpellEffect::PostEditChangeProperty(FPropertyChangedEvent& PropertyChangedEvent)
{
    Super::PostEditChangeProperty(PropertyChangedEvent);

    if (PropertyChangedEvent.GetPropertyName() == "BaseValue")
    {
        Modifier.Initialize();
    }
}

void USpellEffect::AddAllParametersTo(TArray<FFloatParameter*>& Params)
{
    if (Modifier.GetGameplayTag().IsValid())
    {
        Params.Add(&Modifier);
    }
}

void USpellEffect::RemoveParametersFrom(TArray<FFloatParameter*>& Params)
{
    Params.RemoveSwap(&Modifier);
}

void USpellEffect::HandleAffect(FAffectingInfo const& AffectingInfo)
{
    FFloatParameter& Param = AffectingInfo.ChangedParameter;

    if (Modifier.GetValue() && Param.GetMultiplying() > 0.f)
    {
        switch (GetAffectingType())
        {
            case EAffectingType::Add:
            {
                Param.SetBaseValue(Param.GetBaseValue() + (Modifier.GetValue() / Param.GetMultiplying()));
                Param += Modifier.GetValue();
                break;
            }
            case EAffectingType::Multiply:
            {
                Param.SetBaseValue(Param.GetBaseValue() * Modifier.GetValue());
                Param *= Modifier.GetValue();
                break;
            }
        }

        Param.GetAfterChangeDelegate().Broadcast(AffectingInfo);
    }
}

void USpellEffect::BeginPlay(UWorld* World)
{
    Super::BeginPlay(World);
}
