// Fill out your copyright notice in the Description page of Project Settings.


#include "SpellCastManagerComponent.h"
#include "DurationEffect.h"
#include "Net/UnrealNetwork.h"
#include "Classes/Engine/ActorChannel.h"

// Sets default values for this component's properties
USpellCastManagerComponent::USpellCastManagerComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = false;
    SetIsReplicatedByDefault(true);

	// ...
}

void USpellCastManagerComponent::PostEditChangeProperty(struct FPropertyChangedEvent& PropertyChangedEvent)
{
    if (PropertyChangedEvent.GetPropertyName() == "BaseValue")
    {
        for (auto& Stat : Stats)
        {
            Stat.Initialize();
        }
    }
}

bool USpellCastManagerComponent::ReplicateSubobjects(UActorChannel *Channel, FOutBunch *Bunch, FReplicationFlags *RepFlags)
{
    bool bWroteSomething = Super::ReplicateSubobjects(Channel, Bunch, RepFlags);

    for (auto& Itr : AppliedEffects)
    {
        bWroteSomething |= Channel->ReplicateSubobject(Itr, *Bunch, *RepFlags);
    }

    return bWroteSomething;
}

void USpellCastManagerComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
    Super::GetLifetimeReplicatedProps(OutLifetimeProps);

    DOREPLIFETIME(USpellCastManagerComponent, Stats);
    DOREPLIFETIME(USpellCastManagerComponent, AppliedEffects);
}

// Called when the game starts
void USpellCastManagerComponent::BeginPlay()
{
    Super::BeginPlay();

    for (auto& Stat : Stats)
    {
        if (Stat.ParameterName.IsValid())
        {
            AllParametersWithTags.Add(&Stat);
        }
    }
}

UPermanentEffect* USpellCastManagerComponent::CreateAuraEffect(UClass const* DefaultClass, USpellCastManagerComponent* EffectCaster)
{
    UPermanentEffect* NewEffect = NewObject<UPermanentEffect>(this, DefaultClass);
    NewEffect->SetCaster(EffectCaster);
    NewEffect->SetOwner(this);

    return NewEffect;
}

void USpellCastManagerComponent::ApplyEffect_Implementation(USpellCastManagerComponent* Caster, UClass* EffectClass)
{
    if (!Caster || !EffectClass)
    {
        return;
    }

    if (USpellEffect* Effect = EffectClass->GetDefaultObject<USpellEffect>())
    {
        if (!Effect->IsInstant())
        {
            Effect = CreateAuraEffect(EffectClass, this);
            Effect->AddAllParametersTo(AllParametersWithTags);
            AppliedEffects.Add(Effect);
        }

        for (auto& Parameter : AllParametersWithTags)
        {
            if (Parameter->ParameterName.MatchesAny(Effect->GetAffectingTag()))
            {
                FAffectingInfo AffectingInfo(this, Caster, *Parameter, Effect, true);
                Effect->HandleAffect(AffectingInfo);
            }
        }
    }
}

void USpellCastManagerComponent::RemoveEffect_Implementation(UPermanentEffect* Effect)
{
    if (!Effect)
    {
        return;
    }

    Effect->RemoveParametersFrom(AllParametersWithTags);
    AppliedEffects.RemoveSwap(Effect);

    for (auto& Parameter : AllParametersWithTags)
    {
        if (Parameter->ParameterName.MatchesAny(Effect->GetAffectingTag()))
        {
            FAffectingInfo AffectingInfo(this, Effect->GetCaster(), *Parameter, Effect, false);
            Effect->HandleAffect(AffectingInfo);
        }
    }

    Effect->Destroy();
}

FFloatParameter* USpellCastManagerComponent::FindFirstParameterByTag(FGameplayTag const& Tag)
{
    for (auto& Param : AllParametersWithTags)
    {
        if (Param->ParameterName.MatchesTag(Tag))
        {
            return Param;
        }
    }

    return nullptr;
}

void USpellCastManagerComponent::GetAllParametersByTag(TArray<FFloatParameter*>& ParamList, FGameplayTag const& Tag)
{
    for (auto& Param : AllParametersWithTags)
    {
        if (Param->ParameterName.MatchesTag(Tag))
        {
            ParamList.Add(Param);
        }
    }
}

void USpellCastManagerComponent::GetAllParametersByTagContainer(TArray<FFloatParameter*>& ParamList, FGameplayTagContainer const& Tag)
{
    for (auto& Param : AllParametersWithTags)
    {
        if (Param->ParameterName.MatchesAny(Tag))
        {
            ParamList.Add(Param);
        }
    }
}

void FFloatParameter::ApplyOrRemoveEffect(USpellEffect* Effect, bool bIsApply)
{
    check(Effect);

    switch (Effect->GetAffectingType())
    {
        case EAffectingType::Add:
        {
            bIsApply ? AddingEffects.Add(Effect) : AddingEffects.RemoveSwap(Effect);

            Adding = 0.f;

            for (auto& Itr : AddingEffects)
            {
                Adding += Itr->GetModifierValue();
            }
            break;
        }
        case EAffectingType::Multiply:
        {
            bIsApply ? TotalMultiplyingEffects.Add(Effect) : TotalMultiplyingEffects.RemoveSwap(Effect);

            Multiplying = 1.f;

            for (auto& Itr : TotalMultiplyingEffects)
            {
                Multiplying *= Itr->GetModifierValue();
            }
            break;
        }
    }
}







