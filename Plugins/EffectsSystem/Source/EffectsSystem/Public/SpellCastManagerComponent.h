// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "GameplayTags/Classes/GameplayTagContainer.h"
#include "SpellCastManagerComponent.generated.h"

class USpellEffect;
class UPermanentEffect;
class UDurationEffect;
class USpellCastManagerComponent;

UENUM()
enum class EAffectingType : uint8
{
    Add,
    Multiply,
};

USTRUCT(BlueprintType)
struct FFloatParameter
{
    GENERATED_BODY()

private:

    DECLARE_MULTICAST_DELEGATE_OneParam(FAfterChange, struct FAffectingInfo const&);

    UPROPERTY()
    float Value;

    UPROPERTY(NotReplicated, EditDefaultsOnly)
    float BaseValue;

    UPROPERTY(NotReplicated, EditDefaultsOnly)
    FGameplayTag ParameterName;

    TArray<USpellEffect*> AddingEffects;
    TArray<USpellEffect*> TotalMultiplyingEffects;
    TArray<USpellEffect*> TotalMultiplyingEffectsNotStack;
    
    UPROPERTY()
    float Adding;

    UPROPERTY()
    float Multiplying;

    FAfterChange AfterChangeDelegate;


public:

    FFloatParameter() : Multiplying(1.f) {}

    FORCEINLINE void Initialize() { Value = BaseValue; }

    FORCEINLINE const FGameplayTag& GetGameplayTag() const { return ParameterName; }

    FORCEINLINE float GetValue()       const { return       Value; }
    FORCEINLINE float GetBaseValue()   const { return   BaseValue; }

    FORCEINLINE float GetAdding()      const { return      Adding; }
    FORCEINLINE float GetMultiplying() const { return Multiplying; }

    FORCEINLINE void SetValue    (float NewValue) { Value     = NewValue; }
    FORCEINLINE void SetBaseValue(float NewValue) { BaseValue = NewValue; }

    void ApplyOrRemoveEffect(USpellEffect* Effect, bool bIsApply);

    FORCEINLINE FAfterChange& GetAfterChangeDelegate() { return AfterChangeDelegate; };

    FORCEINLINE void Recalculate()
    {
        Value = (BaseValue + Adding) * Multiplying;
    }

    FORCEINLINE bool operator >  (float Val) const { return Value >  Val; }
    FORCEINLINE bool operator == (float Val) const { return Value == Val; }
    FORCEINLINE bool operator <  (float Val) const { return Value <  Val; }
    FORCEINLINE void operator =  (float Val)       {        Value =  Val; }
    FORCEINLINE void operator *= (float Val)       {        Value *= Val; }
    FORCEINLINE void operator -= (float Val)       {        Value -= Val; }
    FORCEINLINE void operator /= (float Val)       {        Value /= Val; }
    FORCEINLINE void operator += (float Val)       {        Value += Val; }

    friend class USpellCastManagerComponent;
};

struct FAffectingInfo
{
    FAffectingInfo
    (
        USpellCastManagerComponent const* NewOwner,
        USpellCastManagerComponent const* NewInitiator,
        FFloatParameter& NewChangedParameter,
        USpellEffect const* NewEffect,
        bool bIsApply
    )
        : Owner(NewOwner), Initiator(NewInitiator), ChangedParameter(NewChangedParameter), Effect(NewEffect), bIsApplying(bIsApply)
    {
        OldValue = ChangedParameter.GetValue();
    }


    USpellCastManagerComponent const* Owner;
    USpellCastManagerComponent const* Initiator;
    FFloatParameter& ChangedParameter;
    USpellEffect const* Effect;
    float OldValue;
    bool bIsApplying;
};

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class EFFECTSSYSTEM_API USpellCastManagerComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	USpellCastManagerComponent();

    UFUNCTION(NetMulticast, Reliable)
    void ApplyEffect(USpellCastManagerComponent* Caster, UClass* EffectClass);

    UFUNCTION(NetMulticast, Reliable)
    void RemoveEffect(UPermanentEffect* Effect);

    FFloatParameter* FindFirstParameterByTag(FGameplayTag const& Tag);

    void GetAllParametersByTag         (TArray<FFloatParameter*>& ParamList, FGameplayTag          const& Tag);
    void GetAllParametersByTagContainer(TArray<FFloatParameter*>& ParamList, FGameplayTagContainer const& Tag);

    virtual void PostEditChangeProperty(struct FPropertyChangedEvent& PropertyChangedEvent) override;

    virtual bool ReplicateSubobjects(class UActorChannel *Channel, class FOutBunch *Bunch, FReplicationFlags *RepFlags) override;

protected:

    TArray<FFloatParameter*> AllParametersWithTags;

	// Called when the game starts
	virtual void BeginPlay() override;

    UPROPERTY(EditDefaultsOnly)
    FGameplayTag TestTag;
private:

    UPROPERTY(EditAnywhere, Replicated)
    TArray<FFloatParameter> Stats;

    UPROPERTY(Replicated)
    TArray<USpellEffect*> AppliedEffects;

    UPermanentEffect* CreateAuraEffect(UClass const* DefaultClass, USpellCastManagerComponent* EffectCaster);

    virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;
};


