// Fill out your copyright notice in the Description page of Project Settings.


#include "ArenaSpellCastManagerComponent.h"
#include "DurationEffect.h"

void UArenaSpellCastManagerComponent::BeginPlay()
{
    Super::BeginPlay();

    if (GetOwnerRole() == ROLE_Authority)
    {
        for (auto& DefEff : DefaultEffects)
        {
            ApplyEffect(this, DefEff);
        }
    }
}
