// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "SpellCastManagerComponent.h"
#include "ArenaSpellCastManagerComponent.generated.h"

/**
 * 
 */
UCLASS()
class ARENA_API UArenaSpellCastManagerComponent : public USpellCastManagerComponent
{
	GENERATED_BODY()
	

public:

    

private:

    virtual void BeginPlay() override;

    UPROPERTY(EditDefaultsOnly)
    TArray<TSubclassOf<class USpellEffect>> DefaultEffects;
};
