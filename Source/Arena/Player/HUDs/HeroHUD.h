// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "HeroHUD.generated.h"

class AHeroHUD;
class UCurveVector;
class UUserWidget;
struct FFloatingDamageHandler;

/** This is how much time a number can exist on screen in seconds. */
#define FloatingDamage_MaxDuration       1.f

/** This is how much amount numbers can exist on screen at the same time. Max is 255. */
#define FloatingDamage_MaxOnDisplay      20

/** Size of number on screen. */
#define FloatingDamage_DefaultSize       0.4f

/** How much pixels the normal damage text will rise during the animation. */
#define FloatingDamage_AnimationHeight   100.f

/** Curve indexes enum for floating damage animation. */
enum EFDCurve : uint8
{
    /** For appear and disappear. */
    Opacity,

    /** For critical damage impulse animation. */
    Impulse
};

/** Parameters for the floating damage text animation. */
USTRUCT()
struct FFloatingDamageAnimation
{
    GENERATED_BODY()

    friend struct FFloatingDamageHandler;

    /** 
     * Creating new floating damage animation. 
     * 
     * @param  Damage       Damage which we need draw on screen.
     * @param  HitPoint     The 3D coordinate where we need to draw text. (This parameter converting in 2D on our screen later)
     * @param  IsCrit       If is the critical damage. Critical damage has another animation.
     */
    void Create(const uint32& Damage, const FVector& HitPoint, const bool& IsCrit);

    /**
     * Drawing damage text on screen.
     *
     * @param  Canvas           Canvas to Draw HUD on screen.
     * @param  Font             The text font from asset. @see constructor of AHeroHUD().
     * @param  AnimationCurve   Pointer of curve asset for damage animation. @see constructor of AHeroHUD().
     */
    void Draw(UCanvas* Canvas, UFont* Font, UCurveVector* AnimationCurve);

private:

    /** Color of the damage text. */
    FLinearColor    Color;

    /** Drawing timer. */
    float           Duration;

    /** Damage transformed in text. */
    FText           DamageText;

    /** Damage transformed in string. For extracting current text size on screen. */
    FString         DamageTextString;

    /** Where we hit. */
    FVector         HitPosition;

    /** If is the critical damage. Critical damage has another animation. */
    bool            bIsCriticalDamage;
};

/** Class for storing animations. Has general infos: Font, curve for text animation etc... */
USTRUCT()
struct FFloatingDamageHandler
{
    GENERATED_BODY()

    friend class AHeroHUD;

private:

    /**
     * If FloatingDamageInfos is full (we have max numbers on the screen) - we need to delete the oldest animation on screen.
     * CurrentAnimationIndex provides us animation index which we have not used for a long time.
     */
    uint8                       CurrentAnimationIndex;

    /** Pointer of font asset for damage animation. */
    UFont*                      FloatingDamageFont;

    /** Pointer of curve asset for damage animation. */
    UCurveVector*               FloatingDamageAnimationCurve;

    /** Massive of the damage animations. */
    FFloatingDamageAnimation    FloatingDamageInfos[FloatingDamage_MaxOnDisplay];

    /** 
     * Creating new floating damage animation and calculate CurrentAnimationIndex variable.
     * 
     * @see: FFloatingDamageAnimation::Create
     */
    void CreateAnimation(const uint32& Damage, const FVector& HitPoint, const bool& IsCrit);

    /**
     * Updating the drawing animations timer.
     *
     * @param  DeltaSeconds	    Game time elapsed during last frame modified by the time dilation
     */
    void UpdateTheAnimationsDuration(float DeltaSeconds);

    /**
     * Drawing animation on screen.
     *
     * @param  Canvas   Canvas to Draw HUD on screen.
     */
    void DrawAnimations(UCanvas*  Canvas);
};

/**
 * 
 */
UCLASS()
class ARENA_API AHeroHUD : public AHUD
{
    GENERATED_BODY()

    AHeroHUD();

    /** The canvas resource used for drawing is only valid during this event, it will not be valid if drawing functions are called later (e.g. after a Delay node). */
    virtual void DrawHUD() override;

    /** Handle timers of our HUD. */
    virtual void Tick(float DeltaSeconds) override;

    virtual void BeginPlay() override;

    /** Floating damage. */
    FFloatingDamageHandler FloatingDamageHandler;

    /** Pointer for main UMG. */
    UUserWidget* GamePlayWidget;

public:

    /** 
     * Creating new floating damage animation. 
     * 
     * @see: FFloatingDamageAnimation::Create
     */
    void CreateFloatingDamageAnimation(const uint32& Damage, const FVector& HitPoint, const bool& IsCrit);

    FORCEINLINE UUserWidget* GetGamePlayWidget() const { return GamePlayWidget; };

    /** For setups default class in blueprints. */
    UPROPERTY(EditDefaultsOnly, Category = "Health")
    TSubclassOf<UUserWidget> GamePlayWidgetSubclass;
};


