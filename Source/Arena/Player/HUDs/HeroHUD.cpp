// Fill out your copyright notice in the Description page of Project Settings.


#include "HeroHUD.h"

/** For canvas. */
#include "Engine/Canvas.h"

/** For ConstructorHelpers. */
#include "UObject/ConstructorHelpers.h"

/** For FloatingDamageAnimationCurve. */
#include "Curves/CurveVector.h"

/** For UUserWidget. */
#include "Blueprint/UserWidget.h"

//#include "Player/HeroController.h"

#include "Player/Hero.h"

/** The HUD creating only on client side (ROLE_AutonomousProxy). */
AHeroHUD::AHeroHUD()
{
    if (!IsNetMode(NM_DedicatedServer))
    {
        PrimaryActorTick.bCanEverTick = true;
        
        /** Find font in content folder. */
        static ConstructorHelpers::FObjectFinder<UFont> FindFont(TEXT("/Game/Assets/Character/Fonts/Font_FloatingDamage"));

        if (FindFont.Object != nullptr)
        {
            FloatingDamageHandler.FloatingDamageFont = FindFont.Object;
        }

        /** Find curve in content folder. */
        static ConstructorHelpers::FObjectFinder<UCurveVector> FindVectorCurve(TEXT("/Game/Assets/Character/Curves/Curve_FloatingDamage"));

        if (FindVectorCurve.Object != nullptr)
        {
            FloatingDamageHandler.FloatingDamageAnimationCurve = FindVectorCurve.Object;
        }
    }
}

void AHeroHUD::DrawHUD()
{
    if (IsCanvasValid_WarnIfNot())
    {
        FloatingDamageHandler.DrawAnimations(Canvas);
    }
}

/** Call every frame with delta time. */
void AHeroHUD::Tick(float DeltaSeconds)
{
    //Super::Tick(DeltaSeconds);

    FloatingDamageHandler.UpdateTheAnimationsDuration(DeltaSeconds);
}

void AHeroHUD::BeginPlay()
{
    Super::BeginPlay();

    if (GamePlayWidgetSubclass)
    {
        /** Create main UMG. */
        GamePlayWidget = CreateWidget<UUserWidget>(GetWorld(), GamePlayWidgetSubclass);

        if (GamePlayWidget)
        {
            GamePlayWidget->AddToViewport();
        }
    }
}

void AHeroHUD::CreateFloatingDamageAnimation(const uint32& Damage, const FVector& HitPoint, const bool& IsCrit)
{
    FloatingDamageHandler.CreateAnimation(Damage, HitPoint, IsCrit);
}

void FFloatingDamageHandler::CreateAnimation(const uint32& Damage, const FVector& HitPoint, const bool& IsCrit)
{
    FloatingDamageInfos[CurrentAnimationIndex++].Create(Damage, HitPoint, IsCrit);

    if (CurrentAnimationIndex >= FloatingDamage_MaxOnDisplay)
    {
        /** Set 0 if our index more than limit. */
        CurrentAnimationIndex = 0;
    }
}

void FFloatingDamageHandler::UpdateTheAnimationsDuration( float DeltaSeconds)
{
    for (uint8 i = 0; i < FloatingDamage_MaxOnDisplay; i++)
    {
        if (!FloatingDamageInfos[i].Duration)
        {
            /** We don't need to update, if he don't have the duration. */
            continue;
        }

        FloatingDamageInfos[i].Duration -= DeltaSeconds;

        if (FloatingDamageInfos[i].Duration < 0.f)
        {
            FloatingDamageInfos[i].Duration = 0.f;
        }
    }
}

void FFloatingDamageHandler::DrawAnimations(UCanvas*  Canvas)
{
    /** Canvas checked outside in Tick() function . */

    for (uint8 i = 0; i < FloatingDamage_MaxOnDisplay; i++)
    {
        if (!FloatingDamageInfos[i].Duration)
        {
            /** We don't need to draw, if he don't have the duration. */
            continue;
        }
        FloatingDamageInfos[i].Draw(Canvas, FloatingDamageFont, FloatingDamageAnimationCurve);
    }
}

void FFloatingDamageAnimation::Create(const uint32 & Damage, const FVector & HitPoint, const bool & IsCrit)
{
    /** Orange */
    Color             = FLinearColor(1.f, 0.64f, 0.f);

    /** Convert damage number to text. */
    DamageText        = FText::AsNumber(Damage);

    /** Convert damage text to string. */
    DamageTextString  = DamageText.ToString();


    HitPosition.X     = HitPoint.X;
    HitPosition.Y     = HitPoint.Y;

    /** 
     * Add some height, because text mast be appear a little higher than the victims.
     * TODO: Calculate actor height. (maybe use via AActor::GetActorBounds)
     */
    HitPosition.Z     = HitPoint.Z + 90.f;
    Duration          = FloatingDamage_MaxDuration;
    bIsCriticalDamage = IsCrit;
}

void FFloatingDamageAnimation::Draw(UCanvas* Canvas, UFont*  Font, UCurveVector*  AnimationCurve)
{
    /** Transforms our 3D world space vector to 2D screen coordinates. */
    FVector Location2D = Canvas->Project(HitPosition);

    /** TODO: Fix check X and Y coordinates. */
    if (!Location2D.Z)
    {
        /** We don't need to handle, if number is behind the screen. */
        return;
    }

    float Scale = FloatingDamage_DefaultSize;

    /** When 1.0f is 100% */
    float DurationPercent = Duration / FloatingDamage_MaxDuration;

    if (bIsCriticalDamage)
    {
        /** Critical damage impulse animation. Extracting float value from curve asset. */
        Scale *= AnimationCurve->FloatCurves[EFDCurve::Impulse].Eval(DurationPercent);
    }
    else
    {
        /** For animation of the normal damage. */
        Location2D.Y -= FloatingDamage_AnimationHeight - FloatingDamage_AnimationHeight * DurationPercent;
    }

    float OutWidth = 0.f, OutHeight = 0.f;

    /** Extracting text size on screen. This is for text pivot. */
    Canvas->TextSize(Font, DamageTextString, OutWidth, OutHeight, Scale, Scale);

    /** The pivot is on down center. */
    Location2D.X -= OutWidth / 2.f;
    Location2D.Y -= OutHeight;

    /** Edit alpha channel for text opacity. The text must be appear and disappear smoothly. */
    Color.A = AnimationCurve->FloatCurves[EFDCurve::Opacity].Eval(DurationPercent);

    FCanvasTextItem TextItem(FVector2D(Location2D), DamageText, Font, Color);

    /** TODO: The screen size must be affect on the text scale. */
    TextItem.Scale = FVector2D(Scale, Scale);

    /** Drawing the damage text on screen. */
    Canvas->DrawItem(TextItem);
}

