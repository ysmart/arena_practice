// Fill out your copyright notice in the Description page of Project Settings.


#include "GamePlayWidget.h"
#include "UMG/Public/Components/ProgressBar.h"
#include "UMG/Public/Components/TextBlock.h"


void UGamePlayWidget::NativeOnInitialized()
{
    PBCastBar->SetVisibility(ESlateVisibility::Collapsed);
    PBCastBar->PercentDelegate.BindUFunction(this, "GetCastBarProgress");
    PBCastBar->VisibilityDelegate.BindUFunction(this, "GetCastBarVisibility");
    DefaultFillColor = PBCastBar->FillColorAndOpacity;

    FOnTimelineEventStatic NewTimelineFinishedFunc;
    NewTimelineFinishedFunc.BindUFunction(this, "ResetCastBarColor");
    CastBarAnimation.SetTimelineFinishedFunc(NewTimelineFinishedFunc);
    CastBarAnimation.SetTimelineLength(0.3f);

    if (CastBarSuccessful)
    {
        FOnTimelineLinearColor NewTimelinePostUpdateFunc;
        NewTimelinePostUpdateFunc.BindUFunction(this, "CastBarAnimationTick");
        CastBarAnimation.AddInterpLinearColor(CastBarSuccessful, NewTimelinePostUpdateFunc, NAME_None, "CastResult");
    }
}

void UGamePlayWidget::InitializingDelegates(USpellCastManagerComponent* SpellCastManagerComponent)
{
    check(SpellCastManagerComponent);

    if (FFloatParameter* HealthParam = SpellCastManagerComponent->FindFirstParameterByTag(CurrentHealthTag))
    {
        CurrentHealth = HealthParam->GetValue();
        HealthParam->GetAfterChangeDelegate().AddUObject(this, &UGamePlayWidget::UpdateCurrentHealht);
    }

    if (FFloatParameter* MaxHealthParam = SpellCastManagerComponent->FindFirstParameterByTag(MaxHealthTag))
    {
        MaxHealth = MaxHealthParam->GetValue();
        MaxHealthParam->GetAfterChangeDelegate().AddUObject(this, &UGamePlayWidget::UpdateMaxHealht);
    }

    if (FFloatParameter* EnergyParam = SpellCastManagerComponent->FindFirstParameterByTag(CurrentEnergyTag))
    {
        CurrentEnergy = EnergyParam->GetValue();
        EnergyParam->GetAfterChangeDelegate().AddUObject(this, &UGamePlayWidget::UpdateCurrentEnergy);
    }

    if (FFloatParameter* MaxEnergyParam = SpellCastManagerComponent->FindFirstParameterByTag(MaxEnergyTag))
    {
        MaxEnergy = MaxEnergyParam->GetValue();
        MaxEnergyParam->GetAfterChangeDelegate().AddUObject(this, &UGamePlayWidget::UpdateMaxEnergy);
    }

    UpdateHealhtBar();
    UpdateEnergyBar();
}

void UGamePlayWidget::SetStatusElementsVisibility(ESlateVisibility SlateVisibility, UProgressBar * ProgressBar, UTextBlock * TextBlock, UTextBlock * TextBlockPct)
{
    ProgressBar->SetVisibility(SlateVisibility);
    TextBlock->SetVisibility(SlateVisibility);
    TextBlockPct->SetVisibility(SlateVisibility);
}

void UGamePlayWidget::PlayCastBarSuccessfulAnimation(uint32)
{
    PlayCastBarAnimation(CastBarSuccessful);
}

void UGamePlayWidget::PlayCastBarCancelAnimation()
{
    PlayCastBarAnimation(CastBarCancel);
}

bool UGamePlayWidget::IsPlayingCastBarAnimation() const
{
    return CastBarAnimation.IsPlaying();
}

void UGamePlayWidget::PlayCastBarAnimation(UCurveLinearColor* AnimationCurve)
{
    if (AnimationCurve)
    {
        CastBarAnimation.SetLinearColorCurve(AnimationCurve, "CastResult");
        CastBarAnimation.PlayFromStart();
    }
}

void UGamePlayWidget::NativeTick(const FGeometry & MyGeometry, float InDeltaTime)
{
    CastBarAnimation.TickTimeline(InDeltaTime);
}

void UGamePlayWidget::CastBarAnimationTick(FLinearColor Color)
{
    PBCastBar->SetFillColorAndOpacity(FLinearColor(Color.R, Color.G, Color.B));
    PBCastBar->SetRenderOpacity(Color.A);
}

void UGamePlayWidget::ResetCastBarColor()
{
    CastBarAnimationTick(DefaultFillColor);
}

void UGamePlayWidget::UpdateCurrentHealht(FAffectingInfo const& AffectingInfo)
{
    CurrentHealth = AffectingInfo.ChangedParameter.GetValue();
    UpdateHealhtBar();
}

void UGamePlayWidget::UpdateMaxHealht(FAffectingInfo const& AffectingInfo)
{
    MaxHealth = AffectingInfo.ChangedParameter.GetValue();
    UpdateHealhtBar();
}

void UGamePlayWidget::UpdateCurrentEnergy(FAffectingInfo const& AffectingInfo)
{
    CurrentEnergy = AffectingInfo.ChangedParameter.GetValue();
    UpdateEnergyBar();
}

void UGamePlayWidget::UpdateMaxEnergy(FAffectingInfo const& AffectingInfo)
{
    MaxEnergy = AffectingInfo.ChangedParameter.GetValue();
    UpdateEnergyBar();
}

void UGamePlayWidget::UpdateHealhtBar()
{
    float HealthPct = CurrentHealth / MaxHealth;

    if (PBHealth)
    {
        PBHealth->SetPercent(HealthPct);
    }

    if (TBHealth)
    {
        TBHealth->SetText(FText::FromString(TTypeToString<uint32>::ToString(CurrentHealth)));
    }

    if (TBHealthPct)
    {
        TBHealthPct->SetText(FText::FromString(TTypeToString<uint32>::ToString(HealthPct * 100.f) + "%"));
    }
}

void UGamePlayWidget::UpdateEnergyBar()
{
    float EnergyPct = CurrentEnergy / MaxEnergy;

    if (PBEnergy)
    {
        PBEnergy->SetPercent(EnergyPct);
    }

    if (TBEnergy)
    {
        TBEnergy->SetText(FText::FromString(TTypeToString<uint32>::ToString(CurrentEnergy)));
    }

    if (TBEnergyPct)
    {
        TBEnergyPct->SetText(FText::FromString(TTypeToString<uint32>::ToString(EnergyPct * 100.f) + "%"));
    }
}

void UGamePlayWidget::OnStartCasting(uint32, float)
{
    /** Clear all cast animations of UI. */
    if (IsPlayingCastBarAnimation())
    {
        CastBarAnimation.Stop();
        ResetCastBarColor();
    }
}






