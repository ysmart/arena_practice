// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Components/TimelineComponent.h"
#include "Arena/Spell/ArenaSpellCastManagerComponent.h"
#include "GamePlayWidget.generated.h"

class UProgressBar;
class UTextBlock;

/**
 * 
 */
UCLASS()
class ARENA_API UGamePlayWidget : public UUserWidget
{
    GENERATED_BODY()

public:

    void InitializingDelegates(USpellCastManagerComponent* SpellCastManagerComponent);

    void SetStatusElementsVisibility(ESlateVisibility SlateVisibility, UProgressBar* ProgressBar, UTextBlock* TextBlock, UTextBlock* TextBlockPct);

    bool IsPlayingCastBarAnimation() const;

    virtual void PlayCastBarAnimation(UCurveLinearColor* AnimationCurve);

    /** Call when widget is tick. */
    virtual void NativeTick(const FGeometry& MyGeometry, float InDeltaTime) override;

    virtual void NativeOnInitialized() override;

protected:

    UFUNCTION()
    void CastBarAnimationTick(FLinearColor Color);

    UFUNCTION()
    void ResetCastBarColor();
    

private:

    float MaxHealth;
    float CurrentHealth;

    float MaxEnergy;
    float CurrentEnergy;

    UPROPERTY()
    FTimeline CastBarAnimation;

    /***/
    UPROPERTY(meta = (BindWidget))
    UProgressBar* PBHealth;

    /***/
    UPROPERTY(meta = (BindWidget))
    UTextBlock* TBHealth;

    /***/
    UPROPERTY(meta = (BindWidget))
    UTextBlock* TBHealthPct;

    /***/
    UPROPERTY(meta = (BindWidget))
    UProgressBar* PBEnergy;

    /***/
    UPROPERTY(meta = (BindWidget))
    UTextBlock* TBEnergy;

    /***/
    UPROPERTY(meta = (BindWidget))
    UTextBlock* TBEnergyPct;

    /***/
    UPROPERTY(meta = (BindWidget))
    UProgressBar* PBEnemyHealth;

    /***/
    UPROPERTY(meta = (BindWidget))
    UTextBlock* TBEnemyHealth;

    /***/
    UPROPERTY(meta = (BindWidget))
    UTextBlock* TBEnemyHealthPct;

    /***/
    UPROPERTY(meta = (BindWidget))
    UProgressBar* PBEnemyEnergy;

    /***/
    UPROPERTY(meta = (BindWidget))
    UTextBlock* TBEnemyEnergy;

    /***/
    UPROPERTY(meta = (BindWidget))
    UTextBlock* TBEnemyEnergyPct;

    /***/
    UPROPERTY(meta = (BindWidget))
    UProgressBar* PBCastBar;

    UPROPERTY(EditDefaultsOnly, Category = "CastBar Animation")
    UCurveLinearColor* CastBarSuccessful;

    UPROPERTY(EditDefaultsOnly, Category = "CastBar Animation")
    UCurveLinearColor* CastBarCancel;

    UPROPERTY(EditDefaultsOnly, Category = "Health")
    FGameplayTag CurrentHealthTag;

    UPROPERTY(EditDefaultsOnly, Category = "Health")
    FGameplayTag MaxHealthTag;

    UPROPERTY(EditDefaultsOnly, Category = "Energy")
    FGameplayTag CurrentEnergyTag;

    UPROPERTY(EditDefaultsOnly, Category = "Energy")
    FGameplayTag MaxEnergyTag;

    FLinearColor DefaultFillColor;

    UFUNCTION()
    void OnStartCasting(uint32 SpellID, float CastTime);

    UFUNCTION()
    void PlayCastBarSuccessfulAnimation(uint32 SpellID);

    UFUNCTION()
    void PlayCastBarCancelAnimation();

    void UpdateCurrentHealht(FAffectingInfo const& AffectingInfo);
    void UpdateMaxHealht    (FAffectingInfo const& AffectingInfo);

    void UpdateCurrentEnergy(FAffectingInfo const& AffectingInfo);
    void UpdateMaxEnergy    (FAffectingInfo const& AffectingInfo);

    void UpdateHealhtBar();
    void UpdateEnergyBar();
};
