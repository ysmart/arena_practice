// Fill out your copyright notice in the Description page of Project Settings.


#include "MainMenuWidget.h"
#include "UMG/Public/Components/Button.h"

void UMainMenuWidget::NativeConstruct()
{
    BJoin->OnClicked.AddDynamic(this, &UMainMenuWidget::Join);
    BQuit->OnClicked.AddDynamic(this, &UMainMenuWidget::Quit);
}

void UMainMenuWidget::Join()
{
    /** TODO: Realization. */
}

void UMainMenuWidget::Quit()
{
    /** TODO: Realization. */
}




