// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "MainMenuWidget.generated.h"

class UButton;

/**
 * 
 */
UCLASS()
class ARENA_API UMainMenuWidget : public UUserWidget
{
	GENERATED_BODY()

    virtual void NativeConstruct() override;

    UFUNCTION()
    void Quit();

    UFUNCTION()
    void Join();


public:

    UPROPERTY(EditAnywhere, BlueprintReadOnly, meta = (BindWidget))
    UButton* BQuit;

    UPROPERTY(EditAnywhere, BlueprintReadOnly, meta = (BindWidget))
    UButton* BJoin;
	

};
