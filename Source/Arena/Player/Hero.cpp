// Fill out your copyright notice in the Description page of Project Settings.


#include "Hero.h"

/** For player camera */
#include "GameFramework/SpringArmComponent.h"
#include "Camera/CameraComponent.h"

/** For player input settings */
#include "Components/InputComponent.h"

#include "Components/CapsuleComponent.h"

/** For macros DOREPLIFETIME */
#include "Engine/Public/Net/UnrealNetwork.h"

/** For game play interface. */
#include "Player/HUDs/Widgets/GamePlayWidget.h"

#include "Player/HUDs/HeroHUD.h"
#include "Arena/Spell/ArenaSpellCastManagerComponent.h"
#include "GameFramework\CharacterMovementComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Arena/Player/GamePlayerController.h"


bool AHero::LeftClickPressed_Validate() { return true; }

const FObjectInitializer & AHero::GetHeroInitializer(UWorld const* World, const FObjectInitializer & ObjectInitializer)
{
    if (World && World->IsNetMode(NM_DedicatedServer))
    {
        ObjectInitializer.DoNotCreateDefaultSubobject(AHero::MeshComponentName).DoNotCreateDefaultSubobject("Arrow");
    }

    return ObjectInitializer;
}

/** Sets default values */
AHero::AHero(const FObjectInitializer& ObjectInitializer) : Super(GetHeroInitializer(GetWorld(), ObjectInitializer))
{
 	/** Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it. */
	PrimaryActorTick.bCanEverTick = false;
    bReplicates = true;
    SetReplicatingMovement(true);

    SpellCastManagerComponent = CreateDefaultSubobject<UArenaSpellCastManagerComponent>("SpellCastManagerComponent");

    if (!IsNetMode(NM_DedicatedServer))
    {
        SpringArmComponent = CreateDefaultSubobject<USpringArmComponent>("Arm");
        SpringArmComponent->SetupAttachment(RootComponent);
        SpringArmComponent->bUsePawnControlRotation = false;
        SpringArmComponent->TargetArmLength = 800.f;
        SpringArmComponent->SetRelativeRotation(FRotator(-40.f, 0.f, 0.f));

        CameraComponent = CreateDefaultSubobject<UCameraComponent>("Camera");
        CameraComponent->SetupAttachment(SpringArmComponent);
        CameraComponent->bUsePawnControlRotation = false;
    }
}

/** Called when the game starts or when spawned */
void AHero::BeginPlay()
{
	Super::BeginPlay();

    switch (GetLocalRole())
    {
        case ROLE_AutonomousProxy:
        {
            APlayerController* HeroPlayerController = Cast<APlayerController>(GetController());

            if (HeroPlayerController)
            {
                AHeroHUD* HeroHUD = Cast<AHeroHUD>(HeroPlayerController->GetHUD());

                if (HeroHUD)
                {
                    GamePlayWidget = Cast<UGamePlayWidget>(HeroHUD->GetGamePlayWidget());
                    GamePlayWidget->InitializingDelegates(SpellCastManagerComponent);
                }
            }
            break;
        }
    }
}

void AHero::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
    /** Register replication variable from parent classes */
    Super::GetLifetimeReplicatedProps(OutLifetimeProps);
}

/** Launch on ROLE_AutonomousProxy client side */
void AHero::MoveForward(float Scale)
{
    if (Scale != 0.f)
    {
        AddMovementInput(GetActorForwardVector(), Scale);
    }
}

/** Launch on ROLE_AutonomousProxy client side */
void AHero::MoveRight(float Scale)
{
    if (Scale != 0.f)
    {
        AddMovementInput(GetActorRightVector(), Scale);
    }
}

/** Calling on server. */
void AHero::SetOwner(AActor* NewOwner)
{
    Super::SetOwner(NewOwner);
    SetGamePlayerController(NewOwner);
}

/** Calling only on client. */
void AHero::OnRep_Controller()
{
    Super::OnRep_Controller();
    SetGamePlayerController(GetOwner());
}

void AHero::SetGamePlayerController(AActor* NewOwner)
{
    if (MyPlayerController != NewOwner)
    {
        MyPlayerController = NewOwner ? Cast<AGamePlayerController>(NewOwner) : nullptr;
    }
}

/** Test button. */
void AHero::LeftClickPressed_Implementation()
{
    SpellCastManagerComponent->ApplyEffect(SpellCastManagerComponent, TestEffect);
}

/** 
 * Called to bind functionality to input
 * Launch only on ROLE_AutonomousProxy
 */
void AHero::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

    PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);
    PlayerInputComponent->BindAction("E", IE_Pressed, this, &AHero::LeftClickPressed);

    PlayerInputComponent->BindAxis("MoveForward", this, &AHero::MoveForward);
    PlayerInputComponent->BindAxis("MoveRight", this, &AHero::MoveRight);
}


