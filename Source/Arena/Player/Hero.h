// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "GameplayTags/Classes/GameplayTagContainer.h"
#include "SpellEffect.h"
#include "Hero.generated.h"

class USpringArmComponent;
class UCameraComponent;
class AActor;
class UUserWidget;
class UGamePlayWidget;
class USpellCastManagerComponent;
class AGamePlayerController;

UCLASS()
class ARENA_API AHero : public ACharacter
{
    GENERATED_BODY()

public:
    /** Sets default values for this character's properties */
	AHero(const FObjectInitializer& ObjectInitializer);

protected:

    /** Called when the game starts or when spawned */
	virtual void BeginPlay() override;


private:

    const FObjectInitializer& GetHeroInitializer(UWorld const* World, const FObjectInitializer& ObjectInitializer);

    /**
     * Call on ROLE_AutonomousProxy when player click "W" or "S" buttons.
     * Function register in: SetupPlayerInputComponent().
     * Bind settings in: Project Settings -> Engine -> Input
     *
     * @param  Scale:  Input scalar (1 or -1).
     */
    void MoveForward(float Scale);

    /**
     * Call on ROLE_AutonomousProxy when player click "A" or "D" buttons.
     * Function register in: SetupPlayerInputComponent().
     * Bind settings in: Project Settings -> Engine -> Input
     *
     * @param  Scale:  Input scalar (1 or -1).
     */
    void MoveRight(float Scale);

    virtual void SetOwner(AActor* NewOwner) override;

    virtual void OnRep_Controller() override;

    void SetGamePlayerController(AActor* NewOwner);

    /** Controlled camera position */
    UPROPERTY(EditAnywhere, Category = "Components")
    USpringArmComponent* SpringArmComponent;

    /** Character camera */
    UPROPERTY(EditAnywhere, Category = "Components")
    UCameraComponent* CameraComponent;

    UPROPERTY(EditAnywhere)
    FGameplayTag TestTag;

    UPROPERTY(EditAnywhere)
    TSubclassOf<USpellEffect> TestEffect;

    UPROPERTY(EditAnywhere, Category = "Components")
    USpellCastManagerComponent* SpellCastManagerComponent;

    /** Main interface widget. Health, inventory etc... */
    UPROPERTY()
    UGamePlayWidget* GamePlayWidget;

    UPROPERTY()
    AGamePlayerController* MyPlayerController;

    UFUNCTION(Unreliable, Server, WithValidation)
    void LeftClickPressed();
    
public:	

	/** Called to bind functionality to input */
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

    /** This function need for registration our replicate variables */
    virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;
};



