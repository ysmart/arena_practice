// Fill out your copyright notice in the Description page of Project Settings.


#include "GamePlayerController.h"
#include "GameFramework\PlayerInput.h"
#include "Slate\Public\Widgets\SViewport.h"

AGamePlayerController::AGamePlayerController()
{

}


void AGamePlayerController::BeginPlay()
{
    Super::BeginPlay();

    if (GetLocalRole() == ROLE_AutonomousProxy)
    {
        PlayerCameraManager->bUseClientSideCameraUpdates = false;

        UGameViewportClient* GameViewportClient = GetWorld()->GetGameViewport();

        if (GameViewportClient)
        {
            GameViewportClient->SetMouseLockMode(EMouseLockMode::DoNotLock);
        }
    }
}

void AGamePlayerController::PostInitializeComponents()
{
    AController::PostInitializeComponents();

    if (!IsPendingKill() && (GetNetMode() != NM_Client))
    {
        // create a new player replication info
        InitPlayerState();
    }

    if (GetNetMode() == NM_Client)
    {
        SpawnPlayerCameraManager();
        ResetCameraMode();
        SpawnDefaultHUD();
    }

    AddCheats();

    bPlayerIsWaiting = true;
    StateName = NAME_Spectating; // Don't use ChangeState, because we want to defer spawning the SpectatorPawn until the Player is received
}

void AGamePlayerController::SetupInputComponent()
{
    Super::SetupInputComponent();

    InputComponent->BindAction  ( "RightClick",    IE_Pressed,  this, &AGamePlayerController::RightClickPressed  );
    InputComponent->BindAction  ( "RightClick",    IE_Released, this, &AGamePlayerController::RightClickReleased );
    InputComponent->BindAction  ( "LeftClick",     IE_Pressed,  this, &AGamePlayerController::LeftClickPressed   );
    InputComponent->BindAction  ( "LeftClick",     IE_Released, this, &AGamePlayerController::LeftClickReleased  );
    InputComponent->BindAxis    ( "MousePressing",              this, &AGamePlayerController::MousePressing      );
    InputComponent->BindAxis    ( "Turn",                       this, &AGamePlayerController::AddYawInput        );
    InputComponent->BindAxis    ( "LookUp",                     this, &AGamePlayerController::AddPitchInput      );
}

void AGamePlayerController::RightClickPressed()
{
    SaveLastCursorPositionOnCapture();
    UpdateControllerRotationYaw();
}

void AGamePlayerController::RightClickReleased()
{
    if (!ShouldShowMouseCursor() && !IsInputKeyDown(EKeys::LeftMouseButton))
    {
        ReleaseMouseLockAndCapture();
    }
    UpdateControllerRotationYaw();
}

void AGamePlayerController::LeftClickPressed()
{
    SaveLastCursorPositionOnCapture();
}

void AGamePlayerController::LeftClickReleased()
{
    if (!ShouldShowMouseCursor() && !IsInputKeyDown(EKeys::RightMouseButton))
    {
        ReleaseMouseLockAndCapture();
    }
}

void AGamePlayerController::MousePressing(float Val)
{
    if (!Val || !ShouldShowMouseCursor())
    {
        return;
    }

    float X = 0.f;
    float Y = 0.f;

    if (GetMousePosition(X, Y))
    {
        /** Checking if we try to move cursor when left or right mouse button is pressed. */
        if (FMath::Abs(X - LastCursorPositionBeforeHide.X) < 2.f && FMath::Abs(Y - LastCursorPositionBeforeHide.Y) < 2.f)
        {
            /** If different is not to big we are don't need to capture. */
            return;
        }

        /** Do capture things. */

        SaveLastCursorPositionOnCapture(X, Y);

        ULocalPlayer* LP = Cast<ULocalPlayer>(Player);

        if (LP)
        {
            TSharedPtr<SViewport> ViewportWidgetPtr = LP->ViewportClient->GetGameViewportWidget();

            if (ViewportWidgetPtr.IsValid())
            {
                bShowMouseCursor = false;
                TSharedRef<SViewport> ViewportWidgetRef = ViewportWidgetPtr.ToSharedRef();

                LP->GetSlateOperations().CaptureMouse(ViewportWidgetRef)
                    .LockMouseToWidget(ViewportWidgetRef)
                    .UseHighPrecisionMouseMovement(ViewportWidgetRef);
            }
        }
    }

    UpdateControllerRotationYaw();
}

void AGamePlayerController::AddYawInput(float Val)
{
    if (Val && !ShouldShowMouseCursor())
    {
        Super::AddYawInput(Val);
    }
}

void AGamePlayerController::AddPitchInput(float Val)
{
    if (Val && !ShouldShowMouseCursor())
    {
        Super::AddPitchInput(Val);
    }
}

void AGamePlayerController::SaveLastCursorPositionOnCapture(float& X, float& Y)
{
    LastCursorPositionBeforeHide.X = X;
    LastCursorPositionBeforeHide.Y = Y;
}

void AGamePlayerController::SaveLastCursorPositionOnCapture()
{
    /** We don't need to save it when capture is working and mouse position already saved. */
    if (ShouldShowMouseCursor())
    {
        GetMousePosition(LastCursorPositionBeforeHide.X, LastCursorPositionBeforeHide.Y);
    }
}

void AGamePlayerController::UpdateControllerRotationYaw()
{
    if (GetPawn())
    {
        bool bCanControllerRotationYaw       = !ShouldShowMouseCursor() && IsInputKeyDown(EKeys::RightMouseButton);
        GetPawn()->bUseControllerRotationYaw = bCanControllerRotationYaw;
    }
}

void AGamePlayerController::ReleaseMouseLockAndCapture()
{
    bShowMouseCursor = true;
    SetMouseLocation(LastCursorPositionBeforeHide.X, LastCursorPositionBeforeHide.Y);
    ULocalPlayer* LP = Cast<ULocalPlayer>(Player);

    if (LP)
    {
        LP->GetSlateOperations().ReleaseMouseCapture().ReleaseMouseLock();
    }
}







